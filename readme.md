# Description

A R package for sending emails through gmail, a thin wrapper for gmailR.

# Install

    install.packages(c("rJython", "rJava"))
    install.packages("devtools")
    library(devtools)
    install_github("kindlychung/gmailR")
    install_bitbucket("kindlychung/autoGmail")

# Usage

Just start up R and see the documentation:

    require(autoGmail)
    ?autogmail
